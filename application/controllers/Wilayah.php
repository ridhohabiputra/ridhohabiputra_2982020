<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    /**
    * Tes HumanUp Indonesia
    * @author Ridho Habi Putra
    * 2020
    */

    class Wilayah extends CI_Controller
    {

        function __construct()
        {
            parent::__construct();
        }

        public function kota()
        {
        	$data['title'] = 'Daftar Kota/Kabupaten';
        	$data['kota'] = $this->wilayah_model->daftar_kota();

        	$this->load->view('daftar_kota', $data);
        }

        public function kecamatan()
        {
        	$data['title'] = 'Daftar Kecamatan';
        	$data['kecamatan'] = $this->wilayah_model->daftar_kecamatan();

        	$this->load->view('daftar_kecamatan', $data);
        }

        public function input_kota()
        {

            $data['title'] = 'Input Data Kota/Kabupaten';

            $this->form_validation->set_rules('nama', 'Nama', 'required');
        
            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('input_kota', $data);
            
            }else{

            	$this->wilayah_model->input_kota();
            	$this->session->set_flashdata('success', 'Input Data Kota/Kabupaten.');
                redirect('wilayah/kota');
            }
        }

        public function edit_kota($id)
        {	
        	$data['kota'] = $this->wilayah_model->detail_kota($id);
        	$data['title'] = 'Update Data Kota';

        	$this->form_validation->set_rules('nama', 'Nama', 'required');
            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('update_kota', $data);
            
            }else{

            	$this->wilayah_model->update_kota($id);
            	$this->session->set_flashdata('success', 'Update Data Kota.');
                redirect('wilayah/kota');
            }
        }

        public function hapus_kota($id)
        {
        	$this->wilayah_model->delete_kota($id);
            $this->session->set_flashdata('success', 'Hapus Data Kota/Kabupaten.');
               redirect('wilayah/kota');
        }

        public function input_kecamatan()
        {

            $data['title'] = 'Input Data Kecamatan';
            $data['kota'] 		= $this->wilayah_model->daftar_kota();

            $this->form_validation->set_rules('nama', 'Nama', 'required');
        
            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('input_kecamatan', $data);
            
            }else{

            	$this->wilayah_model->input_kecamatan();
            	$this->session->set_flashdata('success', 'Input Data Kecamatan.');
                redirect('wilayah/kecamatan');
            }
        }

        public function edit_kecamatan($id)
        {	
        	$data['kecamatan'] 	= $this->wilayah_model->detail_kecamatan($id);
        	$data['kota'] 		= $this->wilayah_model->daftar_kota();
        	$data['title'] 		= 'Update Data Kota';

        	$this->form_validation->set_rules('nama', 'Nama', 'required');
            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('update_kecamatan', $data);
            
            }else{

            	$this->wilayah_model->update_kecamatan($id);
            	$this->session->set_flashdata('success', 'Update Data Kecamatan.');
                redirect('wilayah/kecamatan');
            }
        }

        public function hapus_kecamatan($id)
        {
        	$this->wilayah_model->delete_kecamatan($id);
            $this->session->set_flashdata('success', 'Hapus Data Kecamatan.');
               redirect('wilayah/kecamatan');
        }
    }